class Hamming
  def self.compute(a, b)
    if a.length != b.length
      throw ArgumentError
    end
    count = 0
    a.each_char.with_index do |_, index|
      if a[index] != b[index]
        count += 1
      end
    end
    count
  end
end