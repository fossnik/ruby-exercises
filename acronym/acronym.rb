class Acronym
  def self.abbreviate(str)
    array = str.split(/[ -]/)
    remit = ''
    array.each do | word |
      word.each_char.with_index do | char, index |
        if /[[:upper:]]/.match(char) && word != word.upcase || index == 0
          remit += char.upcase
        end
      end
    end
    remit
  end
end