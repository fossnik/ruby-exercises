class Squares
  def initialize(n)
    @number = n
  end

  def square_of_sum
    def factorial(n)
      n == 1 ? 1 : n + factorial(n - 1)
    end

    factorial(@number) ** 2
  end

  def sum_of_squares
    range_object = 1..@number
    range_object.reduce { | a, b | a + b ** 2 }
  end

  def difference
    square_of_sum - sum_of_squares
  end
end