class Affine
  def initialize(a, b)
    def greatest_common_divisor(a, m)
      m == 0 ? a : greatest_common_divisor(m, a % m)
    end

    if greatest_common_divisor(a, 26) != 1
      throw ArgumentError
    end

    @a = a
    @b = b
  end

  def encode(input)
    def split_string(input, width)
      input.split(/(?<=\G.{#{width}})/)
    end

    output = input.gsub(/\W/, '')
        .downcase
        .each_codepoint.map { |codepoint| codepoint.chr =~ /[[:alpha:]]/ ? (((@a * (codepoint - 97) + @b) % 26) + 97).chr : codepoint.chr }.join

    split_string(output, 5).join(' ')
  end

  def decode(input)
    def modular_multiplicative_inverse(a, c)
      0.upto(c - 1).map { |b| (a * b) % c }.index(1)
    end


    mmi_of_a = modular_multiplicative_inverse(@a, 26)
    input.gsub(/\s/, '')
        .each_codepoint.map { |codepoint| codepoint.chr =~ /[[:alpha:]]/ ? (mmi_of_a * (codepoint - 97 + 26 * @b - @b) % 26 + 97).chr : codepoint.chr }.join
  end
end