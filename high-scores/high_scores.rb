class HighScores
  attr_reader :scores

  def initialize(scores)
    @scores = scores
  end

  def latest
    @scores.last
  end

  def personal_best
    @scores.max { |a, b| a <=> b }
  end

  def personal_top_three
    sorted = @scores.sort { |a, b| b <=> a }
    sorted.slice(0..2)
  end
end