class Gigasecond
  def self.from(time)
    time + 10e8
  end
end
