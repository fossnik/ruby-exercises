class Matrix
  def initialize(matrix)
    @matrix = matrix
  end

  def rows
    rows = []
    @matrix.split(/\n/).each_with_index do | row, row_index |
      row.split(' ').each_with_index do | element, column_index |
        temp_row = rows[row_index] || []
        temp_row << element.to_i
        rows[row_index] = temp_row
      end
    end
    rows
  end

  def columns
    columns = []
    @matrix.split(/\n/).each_with_index do | row, row_index |
      row.split(' ').each_with_index do | element, column_index |
        temp_column = columns[column_index] || []
        temp_column << element.to_i
        columns[column_index] = temp_column
      end
    end
    columns
  end
end