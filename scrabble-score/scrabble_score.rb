class Scrabble
  def initialize(input)
    if input.nil?
      @input = ''
    else
      @input = input.upcase.gsub(/[^a-z]/i, '')
    end
  end

  def get_char_value(char)
    values = {
        1 => ['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'],
        2 => ['D', 'G'],
        3 => ['B', 'C', 'M', 'P'],
        4 => ['F', 'H', 'V', 'W', 'Y'],
        5 => ['K'],
        8 => ['J', 'X'],
        10 => ['Q', 'Z']
    }
    values.each do |key, letters|
      if letters.include?(char)
        return key
      end
    end
  end

  def score
    score = 0
    @input.each_char do |char|
      score += get_char_value(char)
    end
    score
  end
end