class IsbnVerifier
  def self.valid?(input)
    if input =~ /[^0-9X -]/
      return false
    end

    if input[0..(input.length - 2)] =~ /X/
      return false
    end

    digits = input.gsub(/[^0-9X]/, '').split('').map { |c| c == 'X' ? 10 : c.to_i }

    if digits.length < 9 || digits.length > 10
      return false
    end

    sum = 0
    digits.reverse.each_with_index do | element, index |
      sum += element * (index + 1)
    end

    sum % 11 == 0
  end
end