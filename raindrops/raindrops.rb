class Raindrops
  def self.convert(str)
    if str % 3 != 0 && str % 5 != 0 && str % 7 != 0
      return str.to_s
    else
      return "#{str % 3 == 0 ? 'Pling' : ''}#{str % 5 == 0 ? 'Plang' : ''}#{str % 7 == 0 ? 'Plong' : ''}"
    end
  end
end