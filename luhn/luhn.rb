module Luhn
  def self.valid?(str)
    # only digits and spaces are valid
    unless str.length > 1 && !str.match(/[^0-9 ]/)
      return false
    end

    # create array of numbers from input
    array = str.gsub(/ /i, '').split('').map(&:to_i)

    if array.length <= 1
      return false
    end

    # every second number is doubled.
    # if the product is greater than 9, subtract 9
    array.each_with_index do | _, index |
      # double second digits
      if index % 2 != 0
        inv_index = array.length - index - 1
        if array[inv_index].to_i * 2 < 10
          array[inv_index] = array[inv_index].to_i * 2
        else
          array[inv_index] = array[inv_index].to_i * 2 - 9
        end
      end
    end

    # sum of all numbers in array
    luhn_sum = array.inject(0, :+)

    luhn_sum % 10 == 0
  end
end