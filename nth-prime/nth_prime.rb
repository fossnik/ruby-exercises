class Prime
  def self.nth(n)
    throw ArgumentError if n < 1

    require 'prime'
    Prime.first(n).last
  end
end