class Year
  def self.leap?(year)
    year % 400 == 0 ? true
        : year % 100 == 0 ? false
              : year % 4 == 0 ? true
                    : false
  end
end