class Pangram
  def self.pangram?(input)
    input.downcase.gsub(/[^a-z]/, '').split('').uniq.length == 26
  end
end