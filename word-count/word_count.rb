class Phrase
  attr_reader :word_tally
  def initialize(str)
    str.strip!
    str.downcase!

    @word_tally = {}
    str.split(/[^[:alpha:]\d'']+/).each do |word|
      if @word_tally.include?(word)
        @word_tally[word] += 1;
      else
        @word_tally[word] = 1;
      end
    end
  end

  @word_tally
end