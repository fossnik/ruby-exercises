class PhoneNumber
  def self.clean(input)
    digits = input.gsub(/\D/, '')

    if digits.length == 11
      return nil if digits[0] != '1'
      digits = digits[1..-1]
    end

    if digits.length == 10
      return nil if digits[0] == '1' || digits[0] == '0'
      return nil if digits[3] == '1' || digits[3] == '0'
      return digits
    else
      return nil
    end
  end
end