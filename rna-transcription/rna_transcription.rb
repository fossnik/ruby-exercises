class Complement
  def self.of_dna(input)
    input.each_char.map { |c| c == 'G' ? 'C' : c == 'C' ? 'G' : c == 'T' ? 'A' : c == 'A' ? 'U' : '' }.join
  end
end