class Atbash
  def self.encode(input)
    output = input.gsub(/\W/, '')
                 .downcase
                 .each_codepoint.map do |codepoint|
      if codepoint.chr =~ /[[:alpha:]]/
        get_char(codepoint)
      else
        codepoint.chr
      end
    end.join

    split_string(output)
  end

  private

  def self.get_char(codepoint)
    zero_index = codepoint - 97
    new_char = (26 - zero_index + 25) % 26
    (new_char + 97).chr
  end

  def self.split_string(output)
    output.split(/(?<=\G.{5})/).join(' ')
  end
end