class Isogram
  def self.isogram?(input)
    letters = []
    input.downcase.gsub(/[^a-z]/i, '').split('').each do |letter|
      if letters.include?(letter)
        return false
      end
      letters << letter
    end
    true
  end
end