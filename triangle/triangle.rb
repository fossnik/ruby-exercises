class Triangle
  def initialize(sides)
    @sides = sides
  end

  def equilateral?
    if is_valid
      @sides.uniq.length == 1
    else
      false
    end
  end

  def isosceles?
    if is_valid
      @sides.uniq.length <= 2
    else
      false
    end
  end

  def scalene?
    if is_valid
      @sides.uniq.length == 3
    else
      false
    end
  end

  private

  def is_valid
    if @sides.min <= 0
      return false
    end

    if @sides[0] >= @sides[1] + @sides[2] || @sides[1] >= @sides[0] + @sides[2] || @sides[2] >= @sides[0] + @sides[1]
      return false
    end

    true
  end
end