class Clock

  attr_reader :hour, :minute

  def initialize(args)
    @hour = args[:hour] || 0
    @minute = args[:minute] || 0
    @hour += @minute / 60
    @hour = (@hour % 24 + 24) % 24
    @minute = (@minute % 60 + 60) % 60
  end

  def to_s
    format("%02d:%02d", hour, minute)
  end

  def + (clock)
    Clock.new(hour: hour + clock.hour, minute: minute + clock.minute)
  end

  def - (clock)
    Clock.new(hour: hour - clock.hour, minute: minute - clock.minute)
  end

  def == (clock)
    self.to_s == clock.to_s
  end
end