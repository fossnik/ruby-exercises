class Series
  def initialize(input)
    @input = input.split('')
  end

  def slices(width)
    if width > @input.length
      throw ArgumentError
    end

    remit = []
    @input.each_with_index do |_, input_index|
      string = ''
      width.times do |adjunct|
        if @input[input_index + adjunct]
          string += @input[input_index + adjunct]
        end
      end
      remit << string
    end
    remit.reject { |element| element.length < width }
  end
end