class Nucleotide
  def self.from_dna(input)
    @histogram = {"A" => 0, "C" => 0, "G" => 0, "T" => 0}

    input.each_char do |char|
      if @histogram.include?(char)
        @histogram[char] += 1
      else
        throw ArgumentError
      end
    end
    self
  end

  def self.count(nucleotide)
    @histogram[nucleotide]
  end

  def self.histogram
    @histogram
  end
end